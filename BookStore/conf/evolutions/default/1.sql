# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table aeronave (
  id_aeronave               integer auto_increment not null,
  piloto                    varchar(255),
  tripulacao                varchar(255),
  constraint pk_aeronave primary key (id_aeronave))
;

create table bilhete (
  id_bilhete                integer auto_increment not null,
  confirmacao               varchar(255),
  cancelamento              varchar(255),
  prorrogacao               varchar(255),
  antecipacao               varchar(255),
  constraint pk_bilhete primary key (id_bilhete))
;

create table escalas (
  id_escalas                integer auto_increment not null,
  cidade                    varchar(255),
  aeroporto                 varchar(255),
  embarque                  varchar(255),
  desembarque               varchar(255),
  constraint pk_escalas primary key (id_escalas))
;

create table passageiro (
  id_passageiro             varchar(255) not null,
  nome                      varchar(255),
  cpf                       varchar(255),
  email                     varchar(255),
  telefone                  varchar(255),
  constraint pk_passageiro primary key (id_passageiro))
;

create table trecho (
  id_trecho                 integer auto_increment not null,
  destino                   varchar(255),
  origem                    varchar(255),
  duracao                   varchar(255),
  constraint pk_trecho primary key (id_trecho))
;

create table voos (
  id_voos                   integer auto_increment not null,
  horario                   varchar(255),
  classe                    varchar(255),
  constraint pk_voos primary key (id_voos))
;

create table check_in (
  assento                   varchar(255) not null,
  data                      varchar(255),
  constraint pk_check_in primary key (assento))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table aeronave;

drop table bilhete;

drop table escalas;

drop table passageiro;

drop table trecho;

drop table voos;

drop table check_in;

SET FOREIGN_KEY_CHECKS=1;

