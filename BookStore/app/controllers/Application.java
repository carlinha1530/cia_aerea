package controllers;


//import Result;



import java.util.List;

import model.dao.AeronaveDAO;
import model.dao.BilheteDAO;
import model.dao.EscalasDAO;
import model.dao.PassageiroDAO;
import model.dao.TrechoDAO;
import model.dao.VoosDAO;
import model.dao.check_inDAO;
import model.vo.Aeronave;
import model.vo.Bilhete;
import model.vo.Escalas;
import model.vo.Passageiro;
import model.vo.Trecho;
import model.vo.Voos;
import model.vo.check_in;
import views.html.*;

import com.fasterxml.jackson.databind.JsonNode;

import play.mvc.*;

public class Application extends Controller {

    public static Result getPassageiro() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new PassageiroDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result savePassageiro() throws Exception {
    	JsonNode json = request().body().asJson();
    	Passageiro c =  JsonObjectParser.Deserialize(json, Passageiro.class);
    	new PassageiroDAO().create(c);
		return ok("saved");
    }
    
    
    public static Result deletePassageiro(String id) throws Exception {
    	System.out.println("DELETE " + id);
    	Passageiro b =  new PassageiroDAO().selectById(id);
    	new PassageiroDAO().delete(b);
    	return play.mvc.Results.redirect("PassageiroList");
    	//return ok("sucesso");
    }    
    public static Result showPassageiro() throws Exception {
    	List<Passageiro> Passageiros  = new PassageiroDAO().selectAll();
    	return ok(passageiro.render(Passageiros));
    }
    
    public static Result newPassageiro() throws Exception {
    	return ok(passageironew.render());
    	//return ok();
    }
    
    public static Result getEscalas() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new EscalasDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result saveEscalas() throws Exception {
    	JsonNode json = request().body().asJson();
    	Escalas d =  JsonObjectParser.Deserialize(json, Escalas.class);
    	new EscalasDAO().create(d);
		return ok("saved");
    }
    
    public static Result deleteEscalas(String id) throws Exception {
    	System.out.println("DELETE " + id);
    	Escalas b =  new EscalasDAO().selectById(id);
    	new EscalasDAO().delete(b);
    	return play.mvc.Results.redirect("EscalasList");
    	//return ok("sucesso");
    }    
    public static Result showEscalas() throws Exception {
    	List<Escalas> Escalas  = new EscalasDAO().selectAll();
    	return ok(escalas.render(Escalas));
    }
    
    public static Result newEscalas() throws Exception {
    	return ok(escalasnew.render());
    	//return ok();
    }
    
    
    
    
    public static Result getAeronave() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new AeronaveDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result saveAeronave() throws Exception {
    	JsonNode json = request().body().asJson();
    	Aeronave e =  JsonObjectParser.Deserialize(json, Aeronave.class);
    	new AeronaveDAO().create(e);
		return ok("saved");
    }
    
    public static Result getBilhete() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new BilheteDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result saveBilhete() throws Exception {
    	JsonNode json = request().body().asJson();
    	Bilhete f =  JsonObjectParser.Deserialize(json, Bilhete.class);
    	new BilheteDAO().create(f);
		return ok("saved");
    }
    
    public static Result getcheck_in() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new check_inDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result savecheck_in() throws Exception {
    	JsonNode json = request().body().asJson();
    	check_in g =  JsonObjectParser.Deserialize(json, check_in.class);
    	new check_inDAO().create(g);
		return ok("saved");
    }
    
    public static Result getTrecho() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new TrechoDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result saveTrecho() throws Exception {
    	JsonNode json = request().body().asJson();
    	Trecho h =  JsonObjectParser.Deserialize(json, Trecho.class);
    	new TrechoDAO().create(h);
		return ok("saved");
    }
    
    
    public static Result getVoos() throws Exception {
    	JsonNode response = JsonObjectParser.Serialize(new VoosDAO().selectAll());
    	System.out.println(response);
        return ok(response);
    }

    public static Result saveVoos() throws Exception {
    	JsonNode json = request().body().asJson();
    	Voos i =  JsonObjectParser.Deserialize(json, Voos.class);
    	new VoosDAO().create(i);
		return ok("saved");
    }
    
    public static Result deleteVoos(String id) throws Exception {
    	System.out.println("DELETE " + id);
    	Voos b =  new VoosDAO().selectById(id);
    	new VoosDAO().delete(b);
    	return play.mvc.Results.redirect("VoosList");
    	//return ok("sucesso");
    }    
    public static Result showVoos() throws Exception {
    	List<Voos> Voos  = new VoosDAO().selectAll();
    	return ok(voos.render(Voos));
    }
    
    public static Result newVoos() throws Exception {
    	return ok(voosnew.render());
    	//return ok();
    }
}
