package model.dao;

import com.avaje.ebean.Ebean;
import java.util.List;
import model.vo.Escalas;

public class EscalasDAO extends BaseDAO<Escalas> {

	public EscalasDAO(Class<Escalas> classType) {
		super(classType);
		// TODO Auto-generated constructor stub
	}
	public EscalasDAO(){
		super(Escalas.class);
		}
	
	public Escalas selectById(String id) throws Exception {
		return Ebean.find(Escalas.class).where().like("IdEscalas", id).findUnique();
    }
}
