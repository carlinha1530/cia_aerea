package model.dao;

import java.util.List;

import com.avaje.ebean.Ebean;

import model.vo.Passageiro;

public class PassageiroDAO<T> extends BaseDAO<Passageiro> {

	public PassageiroDAO(Class<Passageiro> classType) {
		super(classType);
		// TODO Auto-generated constructor stub
	}
	public PassageiroDAO(){
	super(Passageiro.class);
	}
	
	public Passageiro selectById(String id) throws Exception {
		return Ebean.find(Passageiro.class).where().like("IdPassageiro", id).findUnique();
    }
	
}
