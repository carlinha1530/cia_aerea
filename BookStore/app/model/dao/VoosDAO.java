package model.dao;

import java.util.List;
import com.avaje.ebean.Ebean;
import model.vo.Voos;

public class VoosDAO<T> extends BaseDAO<Voos> {

	public VoosDAO(Class<Voos> classType) {
		super(classType);
		// TODO Auto-generated constructor stub
	}
	public VoosDAO(){
		super(Voos.class);
		}
	
	public Voos selectById(String id) throws Exception {
		return Ebean.find(Voos.class).where().like("idVoos", id).findUnique();
    }
}
