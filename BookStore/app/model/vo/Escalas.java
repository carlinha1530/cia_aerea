package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Escalas {
		@Id
		public int idEscalas;
		public String cidade;
		public String aeroporto;
		public String embarque;
		public String desembarque;
		
		public int getIdEscalas() {
			return idEscalas;
		}
		public void setIdEscalas(int idEscalas) {
			this.idEscalas = idEscalas;
		}
		public String getCidade() {
			return cidade;
		}
		public void setCidade(String cidade) {
			this.cidade = cidade;
		}
		public String getAeroporto() {
			return aeroporto;
		}
		public void setAeroporto(String aeroporto) {
			this.aeroporto = aeroporto;
		}
		public String getEmbarque() {
			return embarque;
		}
		public void setEmbarque(String embarque) {
			this.embarque = embarque;
		}
		public String getDesembarque() {
			return desembarque;
		}
		public void setDesembarque(String desembarque) {
			this.desembarque = desembarque;
		}

}
