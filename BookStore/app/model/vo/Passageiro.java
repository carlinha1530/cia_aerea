package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Passageiro {
	@Id
	public String IdPassageiro;
	public String nome;
	public String cpf;
	public String email;
	public String telefone;
	
	public String getIdPassageiro() {
		return IdPassageiro;
	}
	public void setIdPassageiro(String idPassageiro) {
		IdPassageiro = idPassageiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
