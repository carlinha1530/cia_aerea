package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Trecho {
	@Id
	public int idTrecho;
	public String destino;
	public String origem;
	public String duracao;
	
	public int getIdTrecho() {
		return idTrecho;
	}
	public void setIdTrecho(int idTrecho) {
		this.idTrecho = idTrecho;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDuracao() {
		return duracao;
	}
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}

}
