package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Aeronave {
	@Id
	public int idAeronave;
	public String piloto;
	public String tripulacao;
	
	public int getIdAeronave() {
		return idAeronave;
	}
	public void setIdAeronave(int idAeronave) {
		this.idAeronave = idAeronave;
	}
	public String getPiloto() {
		return piloto;
	}
	public void setPiloto(String piloto) {
		this.piloto = piloto;
	}
	public String getTripulacao() {
		return tripulacao;
	}
	public void setTripulacao(String tripulacao) {
		this.tripulacao = tripulacao;
	}
}
