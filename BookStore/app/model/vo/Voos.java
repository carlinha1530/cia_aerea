package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Voos {
	@Id
	public int idVoos;
	public String horario;
	public String classe;
	
	public int getIdVoos() {
		return idVoos;
	}
	public void setIdVoos(int idVoos) {
		this.idVoos = idVoos;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}

}
