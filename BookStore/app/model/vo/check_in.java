package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class check_in {
	@Id
	public String assento;
	public String data;
	
	public String getAssento() {
		return assento;
	}
	public void setAssento(String assento) {
		this.assento = assento;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
}
