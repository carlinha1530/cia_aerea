package model.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bilhete {
	@Id
	public int idBilhete;
	public String confirmacao;
	public String cancelamento;
	public String prorrogacao;
	public String antecipacao;
	
	public int getIdBilhete() {
		return idBilhete;
	}
	public void setIdBilhete(int idBilhete) {
		this.idBilhete = idBilhete;
	}
	public String getConfirmacao() {
		return confirmacao;
	}
	public void setConfirmacao(String confirmacao) {
		this.confirmacao = confirmacao;
	}
	public String getCancelamento() {
		return cancelamento;
	}
	public void setCancelamento(String cancelamento) {
		this.cancelamento = cancelamento;
	}
	public String getProrrogacao() {
		return prorrogacao;
	}
	public void setProrrogacao(String prorrogacao) {
		this.prorrogacao = prorrogacao;
	}
	public String getAntecipacao() {
		return antecipacao;
	}
	public void setAntecipacao(String antecipacao) {
		this.antecipacao = antecipacao;
	}

}
